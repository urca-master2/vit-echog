# CustomDataLoader.py
# Author: EMERY Alexandre
# Date: 31/01/2024

import cv2
import numpy as np
from torchvision import datasets, transforms
from torch.utils.data import DataLoader
from transformers import ViTImageProcessor
from PIL import Image
import matplotlib.pyplot as plt
from Settings import *

class CustomDataLoader:
    def __init__(self, train_dir, test_dir):
        """
        Initialize the custom data loader with train and test directories.
        It also sets up the necessary transformations for the images using 
        a Vision Transformer (ViT) image processor.
        """
        # Initialize the ViT image processor with a pretrained model.
        processor = ViTImageProcessor.from_pretrained(MODEL)
        image_mean, image_std = processor.image_mean, processor.image_std
        size = processor.size["height"]
        
        train_transforms_list = []
        test_transforms_list = []

        # Conditionally add the anisotropic diffusion
        if ENABLED_DIFFUSION:
            transform_function = self.apply_anisotropic_diffusion_cv2 if ENABLED_CV2 else self.apply_anisotropic_diffusion_custom
            train_transforms_list.append(transforms.Lambda(lambda img: transform_function(np.array(img))))
            test_transforms_list.append(transforms.Lambda(lambda img: transform_function(np.array(img))))

        # Add the rest of the transformations
        train_transforms_list.extend([
            transforms.Resize((size, size)),
            transforms.RandomRotation(15),
            transforms.RandomAdjustSharpness(2),
            transforms.ToTensor(),
            transforms.Normalize(mean=image_mean, std=image_std),
        ])

        # Add the rest of the test transformations
        test_transforms_list.extend([
            transforms.Resize((size, size)),
            transforms.ToTensor(),
            transforms.Normalize(mean=image_mean, std=image_std),
        ])
        
        # Create the Compose objects with the lists of transformations
        self.train_transforms = transforms.Compose(train_transforms_list)
        self.test_transforms = transforms.Compose(test_transforms_list)

        # Load the training and testing data
        train_data = datasets.ImageFolder(train_dir, transform=self.train_transforms)
        test_data = datasets.ImageFolder(test_dir, transform=self.test_transforms)

        # Create data loaders for both training and testing data
        self.train_dataloader = DataLoader(train_data, batch_size=4, shuffle=True, num_workers=WORKERS)
        self.test_dataloader = DataLoader(test_data, batch_size=4, shuffle=False, num_workers=WORKERS)

        # Create label to ID and ID to label mappings
        self.label2id = {label: id for id, label in enumerate(train_data.classes)}
        self.id2label = {id: label for label, id in self.label2id.items()}

    @staticmethod
    def apply_anisotropic_diffusion_cv2(image):
        """
        Apply anisotropic diffusion to an image.
        This method uses OpenCV's anisotropicDiffusion function which expects a 2D color (RGB) image.
        """
        # Ensure the image is in RGB format
        if image.ndim == 2:
            image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        elif image.ndim == 3 and image.shape[2] == 1:
            image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        elif image.ndim == 3 and image.shape[2] == 3:
            # Image is already in RGB, no change needed
            pass
        else:
            raise ValueError("Image must be either grayscale (1 channel) or RGB (3 channels).")

        # Normalize the image if it's a floating-point image
        if image.dtype == np.float32:
            if image.max() > 1.0:
                image = image / image.max()  # Normalizing if max value is greater than 1
            image = (image * 255).astype(np.uint8)  # Scaling to [0, 255]

        diffused_image = cv2.ximgproc.anisotropicDiffusion(image, alpha=ALPHA_PARAM, K=K_PARAM, niters=ITER_PARAM)

        # Convert back to PIL image for torchvision transformations
        return Image.fromarray(diffused_image.astype('uint8'))
    
    @staticmethod
    def apply_anisotropic_diffusion_custom(image):
        """
        Applies custom anisotropic diffusion according to Perona and Malik to an image.
        This method first converts the image to grayscale (if it is in RGB), applies diffusion,
        and then converts the resulting grayscale image back to RGB.
        """
        # Convert the image to grayscale if it is in RGB
        if image.ndim == 3 and image.shape[2] == 3:
            image = np.dot(image[..., :3], [0.2989, 0.5870, 0.1140])  # Conversion RGB to Grayscale

        # Convert the image to float for computation
        image = image.astype(np.float32) / 255.0

        for _ in range(ITER_PARAM):
            # Calculate gradients
            grad_north = np.roll(image, -1, axis=0) - image
            grad_south = np.roll(image, 1, axis=0) - image
            grad_east = np.roll(image, -1, axis=1) - image
            grad_west = np.roll(image, 1, axis=1) - image
            
            # Calculate conductance
            c_north = np.exp(-(grad_north/K_PARAM)**2)
            c_south = np.exp(-(grad_south/K_PARAM)**2)
            c_east = np.exp(-(grad_east/K_PARAM)**2)
            c_west = np.exp(-(grad_west/K_PARAM)**2)

            # Apply the diffusion equation
            image += ALPHA_PARAM * (c_north * grad_north + c_south * grad_south + c_east * grad_east + c_west * grad_west)

        # Normalize and convert the image for grayscale return
        image = np.clip(image * 255, 0, 255).astype('uint8')

        # Convert the resulting grayscale image back to RGB
        image_rgb = np.stack([image]*3, axis=-1)

        return Image.fromarray(image_rgb)
    
    def compare_diffusion(self):
        """
        Compare the effect of anisotropic diffusion on an image.
        This function retrieves a batch of images from the DataLoader, 
        selects one image, and displays the original and diffused images side by side.
        """
        # Get a batch of images from the DataLoader
        images, _ = next(iter(self.train_dataloader))

        # Select an image from the batch
        original_image = images[0]
        # Convert PyTorch tensor to numpy array and transpose to HWC format
        np_image = original_image.numpy().transpose((1, 2, 0))

        # Normalize the numpy image if necessary
        if np_image.dtype == np.float32:
            if np_image.max() > 1.0:
                np_image = np_image / np_image.max()  # Normalizing if max value is greater than 1
            np_image = (np_image * 255).astype(np.uint8)  # Scaling to [0, 255]

        # Apply anisotropic diffusion to the image
        transform_function = self.apply_anisotropic_diffusion_cv2 if ENABLED_CV2 else self.apply_anisotropic_diffusion_custom
        diffused_image = transform_function(np_image)

        # Display the original and diffused images side by side for comparison
        fig, axes = plt.subplots(1, 2, figsize=(12, 6))
        axes[0].imshow(np_image)
        axes[0].set_title('Original Image')  # Title for the original image
        axes[0].axis('off')  # Hide axis for a cleaner look

        axes[1].imshow(diffused_image)
        axes[1].set_title('Diffused Image')  # Title for the diffused image
        axes[1].axis('off')  # Hide axis for a cleaner look

        plt.savefig('resultats/image_diffusion_comparison.png')
        print("Image saved in the 'resultats' folder.")
