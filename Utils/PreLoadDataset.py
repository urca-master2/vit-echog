# PreLoadDataset.py
# Author: EMERY Alexandre
# Date: 31/01/2024

import os
import shutil
from sklearn.model_selection import train_test_split

def split_data(source, train_dir, test_dir, split_size=0.8):
    """
    Splits data into training and testing sets.

    Args:
    - source (str): The directory containing the original dataset.
    - train_dir (str): The directory to store the training data.
    - test_dir (str): The directory to store the testing data.
    - split_size (float): The proportion of the dataset to include in the train split.
    """

    # Get a list of files from the source directory
    files = [file for file in os.listdir(source) if os.path.isfile(os.path.join(source, file))]

    # Split the files into training and testing sets
    train_files, test_files = train_test_split(files, test_size=1-split_size)

    # Copy the training files to the training directory
    for file in train_files:
        shutil.copy(os.path.join(source, file), os.path.join(train_dir, file))

    # Copy the testing files to the testing directory
    for file in test_files:
        shutil.copy(os.path.join(source, file), os.path.join(test_dir, file))
        
# Paths for the source directories
yes_source_dir = 'brain_tumor_dataset/yes'
no_source_dir = 'brain_tumor_dataset/no'

# Paths for the train and test directories
train_yes_dir = 'dataset/train/yes'
train_no_dir = 'dataset/train/no'
test_yes_dir = 'dataset/test/yes'
test_no_dir = 'dataset/test/no'

# Create directories if they do not exist
os.makedirs(train_yes_dir, exist_ok=True)
os.makedirs(train_no_dir, exist_ok=True)
os.makedirs(test_yes_dir, exist_ok=True)
os.makedirs(test_no_dir, exist_ok=True)

# Apply the split function to both 'yes' and 'no' datasets
split_data(yes_source_dir, train_yes_dir, test_yes_dir)
split_data(no_source_dir, train_no_dir, test_no_dir)
