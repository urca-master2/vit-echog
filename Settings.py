# Settings.py
# Author: EMERY Alexandre
# Date: 31/01/2024

# Generic Parameters
ENABLED_DIFFUSION = True
ENABLED_TEST_DIFFUSION = True

DATASET_TRAIN_PATH = 'dataset/train'
DATASET_TEST_PATH = 'dataset/test'

WORKERS = 4

# ViT Parameters
STRATEGY = "epoch"
BATCH_SIZE = 32
LEARNING_RATE = 0.00002
NUM_EPOCHS = 2
WEIGHT_DECAY = 0.01

#ViT Model
MODEL = 'google/vit-base-patch16-224-in21k'
# MODEL = 'google/vit-large-patch16-224-in21k'
# MODEL = 'google/vit-base-patch32-224-in21k'
# MODEL = 'google/vit-large-patch32-224-in21k'

#Enabled CV2
ENABLED_CV2 = True

# Anisotrope Diffusion Parameters with
ALPHA_PARAM = 0.02
K_PARAM = 10
ITER_PARAM = 15