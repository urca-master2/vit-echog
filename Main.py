# Main.py
# Author: EMERY Alexandre
# Date: 31/01/2024

# Import necessary modules from different packages.
from CustomDataLoader import CustomDataLoader
from Model.Model import CustomModel
from Train.Train import TrainModel
from Train.Evaluate import EvaluateModel
from Settings import *

def main():
    """
    The main function of the script.
    It performs the following operations:
    1. Loads training and testing data.
    2. Initializes the custom model.
    3. Trains the model.
    4. Evaluates the model and prints metrics.
    """

    # Directories containing the training and testing datasets.
    train_dir = DATASET_TRAIN_PATH
    test_dir = DATASET_TEST_PATH

    # Initialize the custom data loader with the dataset directories.
    data_loader = CustomDataLoader(train_dir, test_dir)
    
    # Compares different diffusion techniques (if applicable) in data loading.
    if ENABLED_TEST_DIFFUSION:
        data_loader.compare_diffusion()

    # Create an instance of the custom model with label mappings.
    model = CustomModel(data_loader.label2id, data_loader.id2label).model

    # Initialize the training process with the model and datasets.
    train_model = TrainModel(model, data_loader.train_dataloader.dataset, data_loader.test_dataloader.dataset, data_loader.label2id)
    
    # Starts the training process.
    train_model.train()

    # Initialize the evaluation process with the trained model.
    training_history = train_model.get_training_history()
    evaluate_model = EvaluateModel(train_model.trainer, data_loader.test_dataloader.dataset)
    
    # Run prediction and print evaluation metrics.
    evaluate_model.predict_and_print_metrics()
    evaluate_model.plot_accuracy_graph(training_history)
    evaluate_model.plot_confusion_matrix()

if __name__ == "__main__":
    # Entry point of the script.
    main()
