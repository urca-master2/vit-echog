# ViT-EchoG + Anisotrope Diffusion

## Auteur
**EMERY Alexandre**

## Date
*31/01/2024*

## Description
"ViT-EchoG + Anisotrope Diffusion" est un projet basé sur Python qui utilise des Transformers Visuels (ViT) pour des tâches de classification d'images, enrichi par des techniques de diffusion anisotrope pour le prétraitement des images. Ce projet vise à intégrer des techniques avancées de traitement d'images avec des modèles d'apprentissage profond modernes.

---

## Prérequis
- Python 3
- Pip 3

## Installation

Pour installer et exécuter "ViT-EchoG + Anisotrope Diffusion", suivez ces étapes :

Suivez ces étapes pour lancer l'application :

1. **Installer Python 3**  
   Assurez-vous que Python 3 est installé sur votre système.

2. **Télécharger le code source**  
   Clonez ou téléchargez le code source du projet.

3. **Ouvrir un terminal**  
   Naviguez jusqu'au répertoire du projet.

4. **Naviguer**  
Naviguer dans votre terminal linux ou votre wsl jusqu'à la racine du projet

5. **Lancer l'application**  
Exécutez le script `run.sh` : sh run.sh

## Configuration
Modifiez le comportement de la simulation en ajustant les paramètres dans `Settings.py`. Vous pourrez choisir le model ViT souhaitez ainsi qu'un grand panel de champs ajustables dont l'activation du prétraitement par diffusion anisotropique.

**Paramètres ajustables :**
- `ENABLED_DIFFUSION`
- `ENABLED_TEST_DIFFUSION`

- `DATASET_TRAIN_PATH`
- `DATASET_TEST_PATH`

- `WORKERS`

- `STRATEGY`
- `BATCH_SIZE`
- `LEARNING_RATE`
- `NUM_EPOCHS`
- `WEIGHT_DECAY`

- `MODEL`

- `ENABLED_CV2`

- `ALPHA_PARAM`
- `K_PARAM`
- `ITER_PARAM`

---

## Démarrage de l'Application
Après configuration, lancez l'application avec :
sh run.sh

## Support
Pour toute question ou problème technique, veuillez contacter :  
**alexandreemery@etudiant.univ-reims.fr**