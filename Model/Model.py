# Model.py
# Author: EMERY Alexandre
# Date: 31/01/2024

from transformers import ViTForImageClassification

from Settings import *

class CustomModel:
    def __init__(self, label2id, id2label):
        """
        Initialize the CustomModel class with label-to-ID and ID-to-label mappings.
        This class creates a Vision Transformer (ViT) model for image classification.

        Args:
        - label2id (dict): A dictionary mapping labels to their corresponding IDs.
        - id2label (dict): A dictionary mapping IDs to their corresponding labels.
        """
        # Load a pretrained Vision Transformer model from Hugging Face.
        # The model is fine-tuned for image classification tasks.
        self.model = ViTForImageClassification.from_pretrained(
            MODEL,  # Pretrained model identifier
            id2label=id2label,  # Mapping from ID to label
            label2id=label2id   # Mapping from label to ID
        )
