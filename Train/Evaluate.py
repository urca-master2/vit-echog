# Evaluate.py
# Author: EMERY Alexandre
# Date: 31/01/2024

from tabulate import tabulate
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

class EvaluateModel:
    def __init__(self, trainer, test_data):
        """
        Initialize the EvaluateModel class with a Trainer and test dataset.

        Args:
        - trainer: A Trainer object from the Hugging Face library, already configured for training.
        - test_data: The dataset used for evaluating the model.
        """
        self.trainer = trainer
        self.test_data = test_data

    def predict_and_print_metrics(self):
        """
        Predicts using the trainer on the test dataset and prints out the evaluation metrics.
        This function uses the 'predict' method of the Trainer class and formats the output metrics
        using the 'tabulate' library for better readability.
        """
        # Perform prediction on the test dataset
        outputs = self.trainer.predict(self.test_data)

        # Extract the computed metrics from the outputs
        metrics = outputs.metrics
            
        # Prepare data for tabulation
        data = [[key, value] for key, value in metrics.items()]

        # Print the metrics in a tabulated format
        print(tabulate(data, headers=["Metric", "Value"]))
        
    def plot_accuracy_graph(self, training_history):
        """
        Plots the training accuracy and loss over epochs.

        Args:
            training_history (dict): A dictionary containing the training history. 
                                    This includes the 'accuracy' and 'loss' metrics 
                                    across epochs.

        Saves the plot as an image in the 'resultats' directory and prints a 
        confirmation message.
        """
        epochs = range(1, len(training_history['loss']) + 1)
        plt.plot(epochs, training_history['accuracy'], 'b', label='Accuracy')  # Plot accuracy over epochs in blue
        plt.plot(epochs, training_history['loss'], 'r', label='Loss')  # Plot loss over epochs in red
        plt.title('Training Accuracy and Loss')  # Set the title of the graph
        plt.xlabel('Epochs')  # Label for the x-axis
        plt.ylabel('Accuracy/Loss')  # Label for the y-axis
        plt.legend()  # Display legend

        plt.savefig('resultats/accuracy-loss-epoch.png')  # Save the plot as an image
        print("Image saved in the 'resultats' folder.")  # Print confirmation message
        
    def plot_confusion_matrix(self):
        """
        Plots the confusion matrix for model predictions on the test dataset.

        This function predicts labels for the test dataset using the trained model,
        calculates the confusion matrix from the true labels and the predictions, and
        plots the confusion matrix using a heatmap.
        
        The resulting plot is saved as an image in the 'resultats' directory and a
        confirmation message is printed.
        """
        # Predict labels on the test dataset
        predictions = self.trainer.predict(self.test_data)
        preds = np.argmax(predictions.predictions, axis=1)

        true_labels = predictions.label_ids

        # Calculate the confusion matrix
        cm = confusion_matrix(true_labels, preds)

        # Display the confusion matrix
        fig, ax = plt.subplots(figsize=(10, 10))
        sns.heatmap(cm, annot=True, fmt='d', ax=ax, cmap='Blues')
        plt.ylabel('Actual')
        plt.xlabel('Predicted')
        plt.title('Confusion Matrix')
        
        plt.savefig('resultats/confusion-matrix.png')
        print("Image sauvegardée dans le dossier resultats.")
