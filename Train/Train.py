# Train.py
# Author: EMERY Alexandre
# Date: 31/01/2024

from transformers import TrainingArguments, Trainer, TrainerCallback
import torch
import numpy as np
from sklearn.metrics import accuracy_score

from Settings import *

class MetricsCallback(TrainerCallback):
    """
    A custom callback to record training metrics at each epoch.

    This callback is designed to be used with the Hugging Face `Trainer` class. It captures and stores the loss and accuracy metrics after each evaluation phase during training. The metrics are stored in a dictionary, allowing for easy access and analysis after training completes.

    Attributes:
        epoch (int): Counter for the number of epochs processed.
        training_history (dict): A dictionary to store the loss and accuracy metrics. Initialized with 'loss' and 'accuracy' keys, each associated with an empty list.

    Methods:
        on_evaluate: Called at the end of the evaluation phase of each epoch to capture and store the evaluation metrics.
        get_history: Returns the recorded training history.
    """

    def __init__(self):
        super().__init__()
        self.epoch = 0
        self.training_history = {'loss': [], 'accuracy': []}

    def on_evaluate(self, args, state, control, metrics=None, **kwargs):
        """
        Called at the end of the evaluation phase of each epoch.

        Captures and stores the evaluation loss and accuracy in the training history.

        Args:
            args: The training arguments.
            state: The state of the training.
            control: Control object to manage training and evaluation flow.
            metrics (dict, optional): A dictionary containing the evaluation metrics.
        """
        if metrics is not None:
            self.training_history['loss'].append(metrics.get('eval_loss', None))
            self.training_history['accuracy'].append(metrics.get('eval_accuracy', None))
        self.epoch += 1

    def get_history(self):
        """
        Returns the recorded training history.

        Returns:
            A dictionary containing the loss and accuracy metrics recorded at each evaluation phase.
        """
        return self.training_history

class TrainModel:
    def __init__(self, model, train_dataset, eval_dataset, label2id):
        """
        Initialize the training setup for a given model with specified datasets.

        Args:
        - model: The model that will be trained.
        - train_dataset: Dataset for training the model.
        - eval_dataset: Dataset for evaluating the model's performance.
        - label2id: Dictionary mapping labels to IDs.
        """   
        self.metrics_callback = MetricsCallback()    
         
        # Setup training arguments.
        # Note: We're not specifying an output directory to avoid creating unwanted directories.
        self.args = TrainingArguments(
            output_dir='/tmp',  # Temporary directory to prevent unnecessary folder creation.
            save_strategy=STRATEGY,
            evaluation_strategy=STRATEGY,
            learning_rate=LEARNING_RATE,
            per_device_train_batch_size=BATCH_SIZE,
            per_device_eval_batch_size=4,
            num_train_epochs=NUM_EPOCHS,
            weight_decay=WEIGHT_DECAY,
            load_best_model_at_end=True,
            metric_for_best_model="accuracy",
            logging_dir='logs',
            remove_unused_columns=False,
        )
        
        # Initialize the Trainer with the model and training arguments.
        self.trainer = Trainer(
            model,
            self.args,
            train_dataset=train_dataset,
            eval_dataset=eval_dataset,
            data_collator=lambda x: {'pixel_values': torch.stack([i[0] for i in x]), 'labels': torch.tensor([i[1] for i in x])},
            compute_metrics=self.compute_metrics,
            callbacks=[self.metrics_callback]
        )

    def compute_metrics(self, eval_pred):
        """
        Compute metrics for evaluating the model.

        Args:
        - eval_pred: Tuple of model predictions and actual labels.

        Returns:
        - A dictionary containing the accuracy metric.
        """
        predictions, labels = eval_pred
        predictions = np.argmax(predictions, axis=1)
        accuracy = accuracy_score(predictions, labels)
        return {'accuracy': accuracy}

    def train(self):
        """
        Starts the training process for the model.
        """
        self.trainer.train()
        
    def get_training_history(self):
        """
        Returns the training history recorded by the MetricsCallback.

        This method retrieves the accumulated training history, including loss and accuracy metrics for each epoch, from an instance of MetricsCallback.

        Returns:
            dict: A dictionary containing the training history with 'loss' and 'accuracy' as keys, each associated with a list of values recorded over epochs.
        """
        return self.metrics_callback.get_history()
